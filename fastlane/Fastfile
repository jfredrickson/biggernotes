# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Don't automatically generate a fastlane README.md
skip_docs

default_platform(:ios)

platform :ios do
  desc "Builds and archives the app for ad-hoc distribution"
  lane :adhoc do
    match(type: "adhoc")
    build_ios_app(
      export_method: "ad-hoc",
      output_directory: "build/adhoc",
      export_options: {
        method: "ad-hoc",
        iCloudContainerEnvironment: "Production"
      }
    )
  end

  desc "Builds and archives the app for development"
  lane :development do
    match(type: "development")
    build_ios_app(
      export_method: "development",
      output_directory: "build/development",
      export_options: {
        method: "development",
        iCloudContainerEnvironment: "Development"
      }
    )
  end

  desc "Builds and archives the app for release"
  lane :release do
    increment_build_number(build_number: ENV['CI_BUILD_ID'])
    match(type: "appstore")
    build_ios_app(
      export_method: "app-store",
      output_directory: "build/release",
      export_options: {
        method: "app-store",
        iCloudContainerEnvironment: "Production"
      }
    )
  end

  desc "Distributes the app for beta testing via TestFlight"
  lane :beta do
    upload_to_testflight(
      skip_waiting_for_build_processing: true,
      ipa: "build/release/BiggerNotes.ipa",
    )
  end
end
