#  BiggerNotes

## Development

### Prerequisites

Initial development environment setup.

#### Ruby gems

```
gem install bundler
bundle install
```

## Building and distribution

### Ad-hoc

First, run an ad-hoc build using Fastlane:

```
bundle exec fastlane adhoc
```

Then to install the app on a device:

1. In Xcode, go to **Window** -> **Devices and Simulators**
2. Select a device
3. Drag and drop the `build/adhoc/BiggerNotes.ipa` file to the Installed Apps list

### TestFlight

Add a version tag to the Git repository and push to GitLab. GitLab CI will build and deploy to TestFlight.
