//
//  Note.swift
//  BiggerNotes
//
//  Created by Jeff Fredrickson on 1/27/20.
//  Copyright © 2020 Jeff Fredrickson. All rights reserved.
//

import Foundation
import CoreData

extension Note {
    var unwrappedModified: Date {
        get { modified ?? Date() }
    }

    var unwrappedContent: String {
        get { content ?? "" }
    }
}
