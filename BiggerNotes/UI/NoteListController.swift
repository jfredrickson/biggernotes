//
//  NoteListController.swift
//  BiggerNotes
//
//  Created by Jeff Fredrickson on 12/25/17.
//  Copyright © 2017 Jeff Fredrickson. All rights reserved.
//

import UIKit
import CoreData

enum Filter {
    static let all: Int = 0
    static let favorites: Int = 1
}

class NoteListController: UITableViewController, NSFetchedResultsControllerDelegate {
    var managedObjectContext: NSManagedObjectContext?
    var addButton: UIBarButtonItem?
    var trashButton: UIBarButtonItem?
    @IBOutlet var filterSegmentedControl: UISegmentedControl?
    var cellSwiped = false

    var selectedFilter: Int {
        if let selectedFilter = filterSegmentedControl?.selectedSegmentIndex {
            return selectedFilter
        } else {
            return Filter.all
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewNote(_:)))
        trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteRows(_:)))

        navigationItem.leftBarButtonItems = [editButtonItem]
        navigationItem.rightBarButtonItems = [addButton!]
    }

    override func viewWillAppear(_ animated: Bool) {
        // If we're coming back from the note detail, delete the note if it's empty
        if let indexPath = tableView.indexPathForSelectedRow {
            let note = fetchedResultsController.object(at: indexPath)
            if note.unwrappedContent.isEmpty {
                managedObjectContext?.delete(note)
            }
        }
        
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func setEditing(_ editing: Bool, animated: Bool) {
        // Don't go into editing mode if it was triggered by a cell swipe.
        if !cellSwiped {
            super.setEditing(editing, animated: animated)
        }

        if editing {
            // When entering edit mode, set detail view's note to nil
            if let navControllers = self.splitViewController?.viewControllers {
                if let detail = navControllers.last?.children.first(where: { $0.isKind(of: NoteDetailController.self)}) as? NoteDetailController {
                    detail.note = nil
                }
            }
            navigationItem.rightBarButtonItems = [trashButton!]
        } else {
            navigationItem.rightBarButtonItems = [addButton!]
        }
    }

    @objc
    func insertNewNote(_ sender: Any) {
        let context = self.fetchedResultsController.managedObjectContext
        let note = Note(context: context)
        note.created = Date()
        note.modified = Date()
        note.content = ""
        note.favorite = false

        if selectedFilter == Filter.favorites {
            note.favorite = true
        }
        
        try! context.save()
        
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        performSegue(withIdentifier: "showDetail", sender: self)
    }

    @IBAction func toggleFavorites(_ sender: UISegmentedControl) {
        NSFetchedResultsController<Note>.deleteCache(withName: "Notes")
        if selectedFilter == Filter.favorites {
            fetchedResultsController.fetchRequest.predicate = NSPredicate(format: "favorite == true")
        } else {
            fetchedResultsController.fetchRequest.predicate = nil
        }
        try! fetchedResultsController.performFetch()
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }

    @objc
    func deleteRows(_ sender: Any) {
        if let selectedRows = tableView.indexPathsForSelectedRows {
            for indexPath in selectedRows {
                fetchedResultsController.managedObjectContext.delete(fetchedResultsController.object(at: indexPath))
            }
        }
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let note = fetchedResultsController.object(at: indexPath)
                let noteDetailController = (segue.destination as! UINavigationController).topViewController as! NoteDetailController
                noteDetailController.note = note
                noteDetailController.managedObjectContext = managedObjectContext
                noteDetailController.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                noteDetailController.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        // Don't perform segues while in edit mode.
        return !self.isEditing
    }
    
    // MARK: - Table View
    
    func configureCell(_ cell: UITableViewCell, withNote note: Note) {
        cell.textLabel!.text = note.content
        cell.detailTextLabel?.text = DateFormatter.localizedString(from: note.unwrappedModified, dateStyle: .medium, timeStyle: .short)
        if note.favorite {
            let star = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            star.image = UIImage(named: "Star")
            star.tintColor = .systemYellow
            cell.accessoryView = star
        } else {
            cell.accessoryView = .none
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let note = fetchedResultsController.object(at: indexPath)
        configureCell(cell, withNote: note)
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let note = fetchedResultsController.object(at: indexPath)
            fetchedResultsController.managedObjectContext.delete(note)
            try! fetchedResultsController.managedObjectContext.save()
        }
    }

    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let note = fetchedResultsController.object(at: indexPath)
        let title = note.favorite ? "Unfavorite" : "Favorite"
        let toggleFavorite = UIContextualAction(style: .normal, title: title) { (_, _, completionHandler) in
            note.favorite = !note.favorite
            do {
                try self.fetchedResultsController.managedObjectContext.save()
            } catch let error {
                self.handle(error)
            }
            completionHandler(true)
        }
        toggleFavorite.backgroundColor = .blue

        let actionsConfiguration = UISwipeActionsConfiguration(actions: [toggleFavorite])
        return actionsConfiguration
    }

    override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        cellSwiped = true
    }

    override func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        cellSwiped = false
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // When done editing a note, if it's empty, delete it.
        let note = fetchedResultsController.object(at: indexPath)
        if note.unwrappedContent.isEmpty {
            managedObjectContext?.delete(note)
        }
    }
    
    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController<Note> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "modified", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Notes")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController

        do {
            try _fetchedResultsController!.performFetch()
        } catch let error {
            handle(error)
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<Note>? = nil

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
            case .insert:
                tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            case .delete:
                tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            default:
                return
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                tableView.insertRows(at: [newIndexPath!], with: .fade)
            case .delete:
                tableView.deleteRows(at: [indexPath!], with: .fade)
            case .update:
                configureCell(tableView.cellForRow(at: indexPath!)!, withNote: anObject as! Note)
            case .move:
                configureCell(tableView.cellForRow(at: indexPath!)!, withNote: anObject as! Note)
                tableView.moveRow(at: indexPath!, to: newIndexPath!)
            default:
                return
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
