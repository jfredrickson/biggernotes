//
//  UIKitExtensions.swift
//  BiggerNotes
//
//  Created by Jeff Fredrickson on 4/12/21.
//  Copyright © 2021 Jeff Fredrickson. All rights reserved.
//

import UIKit

extension UIResponder {
    // Add general error handling
    @objc func handle(_ error: Error, from viewController: UIViewController) {
        guard let nextResponder = next else {
            return assertionFailure("Unhandled error \(error) from \(viewController)")
        }
        
        nextResponder.handle(error, from: viewController)
    }
}

extension UIViewController {
    // For simplicity, UIViewControllers will specify themselves when handling an error
    func handle(_ error: Error) {
        handle(error, from: self)
    }
}
