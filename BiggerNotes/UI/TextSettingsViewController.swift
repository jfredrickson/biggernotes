//
//  TextSettingsViewController.swift
//  BiggerNotes
//
//  Created by Jeff Fredrickson on 1/29/20.
//  Copyright © 2020 Jeff Fredrickson. All rights reserved.
//

import UIKit

class TextSettingsViewController: UIViewController {
    @IBOutlet weak var textSizeSlider: UISlider!

    @IBAction func textSizeSliderChanged(_ sender: Any) {
        UserDefaults.standard.set(textSizeSlider.value, forKey: "textSize")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let textSize = UserDefaults.standard.value(forKey: "textSize") as? Float ?? 36.0
        textSizeSlider.minimumValue = 16.0
        textSizeSlider.maximumValue = 96.0
        textSizeSlider.setValue(textSize, animated: false)
    }
}
