//
//  NoteDetailController.swift
//  BiggerNotes
//
//  Created by Jeff Fredrickson on 12/25/17.
//  Copyright © 2017 Jeff Fredrickson. All rights reserved.
//

import UIKit
import CoreData

class NoteDetailController: UIViewController {
    var note: Note? {
        didSet {
            // Update the UI whenever a note is set
            configureView()
        }
    }

    @IBOutlet weak var noteContentTextView: UITextView!
    var doneButton: UIBarButtonItem?
    var managedObjectContext: NSManagedObjectContext?
    @IBOutlet weak var textSettingsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    func configureView() {
        guard let _ = noteContentTextView, let _ = textSettingsButton else { return }
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem]()
        doneButton = UIBarButtonItem(title: "Done", style: .done, target: nil, action: #selector(doneEditing))
        
        updateTextSize()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTextSize), name: UserDefaults.didChangeNotification, object: UserDefaults.standard)
        
        if let note = note {
            noteContentTextView.isEditable = true
            noteContentTextView.isHidden = false
            noteContentTextView.text = note.content
            showDoneButton()
            textSettingsButton.isHidden = false
            startEditing()
        } else {
            // No note loaded yet (e.g., note list is empty or no note selected yet)
            noteContentTextView.isEditable = false
            noteContentTextView.isHidden = true
            hideDoneButton()
            textSettingsButton.isHidden = true
        }
    }
    
    @objc
    func updateTextSize() {
        let fontSize = UserDefaults.standard.value(forKey: "textSize") as? CGFloat ?? 36.0
        DispatchQueue.main.async {
            let font = self.noteContentTextView.font?.withSize(fontSize)
            self.noteContentTextView.font = font
        }
    }
    
    @objc
    func doneEditing() {
        noteContentTextView.resignFirstResponder()
        hideDoneButton()
    }

    func startEditing() {
        noteContentTextView.becomeFirstResponder()
        showDoneButton()
    }

    func showDoneButton() {
        navigationItem.rightBarButtonItems = [doneButton!]
    }

    func hideDoneButton() {
        navigationItem.rightBarButtonItems = []
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        do {
            if let managedObjectContext = managedObjectContext {
                try managedObjectContext.save()
            }
        } catch let error {
            handle(error)
        }
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    // Adjust layout based on whether the on-screen keyboard is shown or hidden (prevents the keyboard from blocking content)
    @objc
    func keyboardWillChangeFrame(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }

        let keyboardFrameInView = view.convert(keyboardFrame, from: nil)
        let safeAreaFrame = view.safeAreaLayoutGuide.layoutFrame.insetBy(dx: 0, dy: -additionalSafeAreaInsets.bottom)
        let intersection = safeAreaFrame.intersection(keyboardFrameInView)

        let animationDuration: TimeInterval = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)

        UIView.animate(withDuration: animationDuration, delay: 0, options: animationCurve, animations: {
            self.additionalSafeAreaInsets.bottom = intersection.height
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension NoteDetailController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let note = note {
            note.content = textView.text
            note.modified = Date()
        }
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        startEditing()
    }
}

extension NoteDetailController: UIPopoverPresentationControllerDelegate {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTextSettings" {
            let textSettingsViewController = segue.destination
            textSettingsViewController.presentationController?.delegate = self
            textSettingsViewController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: textSettingsButton.frame.size.width, height: textSettingsButton.frame.size.height)
            textSettingsViewController.preferredContentSize = CGSize(width: self.preferredContentSize.width, height: 60)
        }
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
